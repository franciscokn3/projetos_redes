#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <unistd.h>

int main()
{
    int sock_fd;
    struct sockaddr_ll socket_address;
    unsigned char buffer[1500];

    // Préparer le contenu du paquet à envoyer
    memset(buffer, 0xAA, sizeof(buffer));

    // Créer un socket raw
    sock_fd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sock_fd == -1)
    {
        perror("Could not create socket");
        return -1;
    }

    // Configurer l'interface réseau pour le socket
    memset(&socket_address, 0, sizeof(socket_address));
    socket_address.sll_family = AF_PACKET;
    socket_address.sll_protocol = htons(ETH_P_ALL);
    socket_address.sll_ifindex = if_nametoindex("eth0");
    socket_address.sll_halen = ETH_ALEN;
    memcpy(socket_address.sll_addr, "\xAA\xBB\xCC\xDD\xEE\xFF", ETH_ALEN);

    // Envoyer le paquet
    if (sendto(sock_fd, buffer, sizeof(buffer), 0, (struct sockaddr *)&socket_address, sizeof(socket_address)) == -1)
    {
        perror("Error sending packet");
        return -1;
    }

    close(sock_fd);
    return 0;
}

