#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>


int main(int argc, char *argv[]) {
    int sock;
    struct sockaddr_ll endereco;
    char buffer[2048];
    int tam_buffer;
    //int tam_endereco = sizeof(endereco);
    socklen_t tamanho = sizeof(endereco);


    // Cria o socket
    sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sock == -1) {
        printf("Não foi possível criar o socket");
        return 1;
    }
    puts("Socket criado");

    // Prepara o endereço
    endereco.sll_family = AF_PACKET;
    endereco.sll_protocol = htons(ETH_P_ALL);
    endereco.sll_ifindex = if_nametoindex("eth0");
    endereco.sll_hatype = ARPHRD_ETHER;
    endereco.sll_pkttype = PACKET_OTHERHOST;
    endereco.sll_halen = ETH_ALEN;

    
    // Recebe dados do socket
    tam_buffer = recvfrom(sock, buffer, 2048, 0, (struct sockaddr *) &endereco, (socklen_t *)&tamanho);
    if (tam_buffer == -1) {
        printf("Erro ao receber dados");
        return 1;
    }
    printf("Recebidos %d bytes\n", tam_buffer);

    // Imprime os dados recebidos
    for (int i = 0; i < tam_buffer; i++) {
        printf("%02x ", buffer[i]);
        if (i % 16 == 15) {
            printf("\n");
        }
    }
    printf("\n");

    // Fecha o socket
    close(sock);

    return 0;
}
