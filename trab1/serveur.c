#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <unistd.h>

int main()
{
    int sock_fd;
    struct sockaddr_ll socket_address;

    // Créer un socket raw
    sock_fd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sock_fd == -1)
    {
        perror("Could not create socket");
        return -1;
    }

    // Configurer l'interface réseau pour le socket
    memset(&socket_address, 0, sizeof(socket_address));
    socket_address.sll_family = AF_PACKET;
    socket_address.sll_protocol = htons(ETH_P_ALL);
    socket_address.sll_ifindex = if_nametoindex("eth0");

    // Attacher le socket à l'interface réseau
    if (bind(sock_fd, (struct sockaddr *)&socket_address, sizeof(socket_address)) == -1)
    {
        perror("Could not bind socket to interface");
        return -1;
    }

    // Boucle d'écoute pour les paquets entrants
    while (1)
    {
        unsigned char buffer[1500];
        int bytes_received;

        // Recevoir les paquets
        bytes_received = recvfrom(sock_fd, buffer, sizeof(buffer), 0, NULL, 0);
        if (bytes_received == -1)
        {
            perror("Error receiving packets");
            return -1;
        }

        // Afficher le contenu du paquet reçu
        printf("Received packet of size %d\n", bytes_received);
        for (int i = 0; i < bytes_received; i++)
        {
            printf("%02x ", buffer[i]);
        }
        printf("\n");
    }

    close(sock_fd);
    return 0;
}
