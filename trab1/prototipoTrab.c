#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <string.h>
#include <linux/if_arp.h>

#define WINDOW_SIZE 16
#define SEGMENT_SIZE 1024

struct Segment {
    int sequence_number;
    int size;
    char data[SEGMENT_SIZE];
};

int send_segment(int sock_fd, struct Segment segment, struct sockaddr *dest_addr, socklen_t dest_len) {
    int ret = sendto(sock_fd, &segment, sizeof(segment), 0, dest_addr, dest_len);
    if (ret < 0) {
        perror("Error sending segment");
    }
    return ret;
}

int receive_ack(int sock_fd, struct sockaddr *src_addr, socklen_t *src_len) {
    int ack;
    int ret = recvfrom(sock_fd, &ack, sizeof(ack), 0, src_addr, src_len);
    if (ret < 0) {
        perror("Error receiving ACK");
    }
    return ack;
}

int send_file(int sock_fd, FILE *file, struct sockaddr *dest_addr, socklen_t dest_len) {
    int window_start = 0;
    int window_end = window_start + WINDOW_SIZE - 1;

    while (!feof(file)) {
        // Send segments in the current window
        for (int i = window_start; i <= window_end && !feof(file); i++) {
            struct Segment segment;
            segment.sequence_number = i;
            segment.size = fread(segment.data, 1, SEGMENT_SIZE, file);
            send_segment(sock_fd, segment, dest_addr, dest_len);
        }

        // Wait for ACKs
        while (window_start <= window_end) {
            int ack = receive_ack(sock_fd, dest_addr, &dest_len);
            if (ack == window_start) {
                window_start++;
                window_end = window_start + WINDOW_SIZE - 1;
            } else {
                break;
            }
        }
    }

    // Wait for ACKs for remaining segments
    while (window_start <= window_end) {
        int ack = receive_ack(sock_fd, dest_addr, &dest_len);
        if (ack == window_start) {
            window_start++;
            window_end = window_start + WINDOW_SIZE - 1;
        }
    }
}

int main(int argc, char *argv[]) {
    int sock_fd = socket(AF_PACKET, SOCK_RAW, hton(ETH_P_ALL));
    if (sock_fd < 0) {
        perror("Error creating socket");
        exit(EXIT_FAILURE);
    }
    struct sockaddr_ll dest_addr;
    dest_addr.sll_family = AF_PACKET;
    dest_addr.sll_protocol = hton(ETH_P_ALL);
    dest_addr.sll_ifindex = if_nametoindex("eth0");
    dest_addr.sll_hatype = ARPHRD_ETHER;
    dest_addr.sll_pkttype = PACKET_OTHERHOST;
    dest_addr.sll_halen = ETH_ALEN;
    memcpy(dest_addr.sll_addr, "\x00\x00\x00\x00\x00\x00", ETH_ALEN);

    FILE *file = fopen("example.jpg", "rb");
    if (!file) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    send_file(sock_fd, file, (struct sockaddr *) &dest_addr, sizeof(dest_addr));

    fclose(file);
    close(sock_fd);

    return 0;

}
