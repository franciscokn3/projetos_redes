#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>
#include <linux/if_ether.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <errno.h>
#include <net/ethernet.h>
#include<netinet/if_ether.h>	//For ETH_P_ALL
#include<string.h>	//strlen

#include<netinet/ip_icmp.h>	//Provides declarations for icmp header
#include<netinet/udp.h>	//Provides declarations for udp header
#include<netinet/tcp.h>	//Provides declarations for tcp header
#include<netinet/ip.h>	//Provides declarations for ip header


#define Tam_buffer 2048   // tamanho de buffer de recepção e envio "Porta"
#define LEN 4096


int ConexaoRawSocket(char *device)
{
  int soquete;
  struct ifreq ir;
  struct sockaddr_ll endereco;
  struct packet_mreq mr;

  soquete = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));  	/*cria socket*/
    // soquete = socket(AF_PACKET, SOCK_STREAM,0);
  
  if (soquete == -1) {
    printf("Erro no Socket\n");
    exit(-1);
  }

  memset(&ir, 0, sizeof(struct ifreq));  	/*dispositivo eth0*/
  memcpy(ir.ifr_name, device, sizeof(*device));
  if (ioctl(soquete, SIOCGIFINDEX, &ir) == -1) {
    printf("Erro no ioctl\n");
    exit(-1);
  }
	

  memset(&endereco, 0, sizeof(endereco)); 	/*IP do dispositivo*/
  endereco.sll_family = AF_PACKET;
  endereco.sll_protocol = htons(ETH_P_ALL);
  endereco.sll_ifindex = ir.ifr_ifindex;
  if (bind(soquete, (struct sockaddr *)&endereco, sizeof(endereco)) == -1) {
    printf("Erro no bind\n");
    exit(-1);
  }


  memset(&mr, 0, sizeof(mr));          /*Modo Promiscuo*/
  mr.mr_ifindex = ir.ifr_ifindex;
  mr.mr_type = PACKET_MR_PROMISC;
  if (setsockopt(soquete, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)	{
    printf("Erro ao fazer setsockopt\n");
    exit(-1);
  }

  return soquete;
}

int main(){
    // int cliente_port;
    // int cliente_ip;
    int cliente_socket;
    //int server_socket;
    //socklen_t *sll_size;
    int *s;
    char *device ="client" ;
    int connect;
    char buffer[2048];
    int slen;

    struct sockaddr_ll cliente_endereco;

    //criar socket clente e servidor 
    //server_socket = ConexaoRawSocket(device);
    cliente_socket = ConexaoRawSocket(device);

    if(listen(cliente_socket, 5) == -1)
    {
      perror("ERROR Lrecv(cliente_socket,&device,1,0,NULL,NULL)STEN");
      exit(1);
    }
    connect = accept(cliente_socket, (struct sockaddr *)&cliente_endereco, (socklen_t *)&s);
    
    if (connect == -1){
      perror("no accept ");
      exit(1);
    }

    strcpy(buffer,"Welcome!\n\0");
    if (send(cliente_socket,buffer,strlen(buffer), 0)) {
        printf("Aguardando resposta do cliente ...\n");
    
        while (1)
        {
            memset(buffer,0x0, LEN);
        // imprimir mensagem esperando cliente 
            printf("\nServidor aguardando por cliente...\n");
            fflush(stdout);
            
            if((slen = recv(cliente_socket,buffer,LEN,0)) >0){
                buffer[slen] = '\0';
                printf("Mensagem Recebida: %s\n",buffer);

            }

        }
    }
      
      
      printf("%p\n", device);

   

    close(connect);
    printf("Conexão encerrada.\n");


  close(cliente_socket);
  //close(server_socket);
  return 0;
}