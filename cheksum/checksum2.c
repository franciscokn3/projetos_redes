#include <stdio.h>
#include <string.h>

//chechSum algorithm 
unsigned char checksum(unsigned char *data, int len) {
    unsigned char sum = 0;
    int i;
    for (i = 0; i < len; i++) {
        sum += data[i];
    }
    return sum;
}

int main() {
    unsigned char data[] = "dados";
    int len = strlen((char *)data);
    unsigned char sum = checksum(data, len);
    printf("Checksum: %d\n", sum);
    return 0;
}
