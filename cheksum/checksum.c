#include <stdio.h>
#include <stdlib.h>

int main (){
    int i ;
    
    // vetor de bytes para calcular o checksum
    //Conteudo dos dados a serem transmitidps de 8bytes
    unsigned char buffer[] = {0x01,0xFF,0xA4,0xF4,0x12,0x34,0x56,0x77};

    printf("Buffer (hex): ");
    for (i = 0; i < 8; i++){
        printf("%02x", buffer[i] );
    }

    printf("\n");

    //variavel para guardar o checksum 8
    unsigned char checksum = 0; // inicializa o checksum com 0

    //calculo do checksum de 8 bit dos dados
    //quando estora os 255 vai pra zero.. 0...255 
    for (i = 0; i < 8; i++){
        checksum += buffer[i]; // somatorio
    }

    printf("checksum =  %d\n", checksum);

    return 0;
}