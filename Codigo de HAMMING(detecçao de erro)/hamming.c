#include<stdio.h>
#include<math.h>

int r,m,i,j,k,data[100],data_at[100],c,d,number,number_of_errors,error_location;

void error_detect_correct()
{
    number=0;
    for(i=0;i<r;i++)
    {
        if(data[i]!=0)
        {
            for(j=0;j<m;j++)
            {
                if(i==pow(2,j)-1)
                {
                    number=number+data[i];
                }
            }
        }
    }
    number_of_errors=number % 2;
    error_location=0;
    for(i=0;i<r;i++)
    {
        if(data[i]!=0)
        {
            c=0;
            for(j=0;j<m;j++)
            {
                if(i==pow(2,j)-1)
                {
                    c++;
                }
            }
            if(c%2!=0)
            {
                error_location=error_location+pow(2,m-1-c);
            }
        }
    }
    if(number_of_errors==0)
    {
        printf("\n\nNo error in data\n");
    }
    else
    {
        printf("\n\nError detected in data\n");
        printf("Error is in %d bit\n",error_location);
        if(data[r-error_location]==0)
        {
            data[r-error_location]=1;
        }
        else
        {
            data[r-error_location]=0;
        }
        printf("\nData after correction is\n");
        for(i=r-1;i>=0;i--)
        {
            printf("%d",data[i]);
        }
    }
}

//funçao para ler a mensagem original de 'm' bits 
void getdata()
{
    printf("Enter the data bits\n");
    for(i=0;i<m;i++)
    {
        scanf("%d",&data[i]);
    }
    j=0;
    //r = numero de bits redundante necessario 
    // r = log2(m+1) 
    for(i=0;i<r;i++)
    {
        if(pow(2,j)==i+1)
        {
            data_at[r-i-1]=2;
            j++;
        }
        else
        {
            data_at[r-i-1]=data[k];
            k++;
        }
    }
}



//função para adicionar os bits redundantes á mensagem original
void add_redundant_bits()
{
    j=0;
    k=0;
    for(i=0;i<r;i++)
    {
        if(pow(2,j)==i+1)
        {
            data[i]=0;
            j++;
        }
        else
        {
            data[i]=data_at[k];
            k++;
        }
    }
}


// void add_redundant_bits()
// {
//     r = ceil(log2(m + 1)) ;
//     printf("Number of redundant bits: %d\n", r);
//     for (int i = m; i < m + r; i++)
//         data[i] = 0;
// }
// função para transmitir a mensagem com bits redundantes para o destinatario
void transmit_data()
{
    printf("\nData to be transmitted\n");
    for(i=0;i<r;i++)
    {
        printf("%d",data[i]);
    }
    printf("\n");
    printf("Enter the received data bits\n");
    for(i=0;i<r;i++)
    {
        scanf("%d",&data[i]);
    }
}

int main()
{
    printf("Enter the number of data bits\n");
    scanf("%d",&m);
    r=ceil(log2(m))+1;
    printf("Number of redundant bits is %d\n",r);
    getdata();
    add_redundant_bits();
    transmit_data();
    error_detect_correct();
    return 0;
}
