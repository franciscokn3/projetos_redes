#include <stdio.h>
#include <math.h>
#include <string.h>

int m, r, code[30], data[30], rec[30];

void calc_redundant_bits(int data[], int code[], int r) {
  int j, k;
  for (j = 0; j < r; j++) {
    code[(int)pow(2, j) - 1] = data[0];
    for (k = 1; k < m + r; k++) {
      code[(int)pow(2, j) - 1] ^= data[k] & code[(int)pow(2, j) - 1];
    }
  }
  for (j = 0; j < m + r; j++) {
    if (j == pow(2, r) - 1)
      break;
    else
      code[j] = data[j];
  }
}

int main() {
  int i, j, k, count, c, sum = 0;
  printf("Entrez le nombre de bits de données : ");
  scanf("%d", &m);
  r = ceil(log2(m )) + 1;
  
  printf("Nombre de bits redondants: %d\n", r);
  printf("Entrez les bits de données : ");
  for (i = 0; i < m; i++) scanf("%d", &data[i]);
  for (i = m; i < m + r; i++) data[i] = 0;
  calc_redundant_bits(data, code, r);
  printf("Données codées : ");
  for (i = 0; i < m + r; i++) printf("%d", code[i]);
  printf("\nEntrez les bits reçus : ");
  for (i = 0; i < m + r; i++) scanf("%d", &rec[i]);
  for (j = 0; j < r; j++) {
    k = pow(2, j);
    sum = 0;
    for (i = 0; i < m + r; i++) {
      if (i & k) sum++;
    }
    if (sum % 2 != rec[m + j]) {
      count = 0;
      for (c = 0; c < r; c++) {
        if (j != c) count += pow(2, c) * rec[m + c];
      }
      printf("Bit d'erreur à la position %d\n", m + j + count);
    }
  }
  return 0;
}
