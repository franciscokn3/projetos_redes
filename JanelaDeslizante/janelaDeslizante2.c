#include <stdio.h>
#include <stdbool.h>

#define JANELA_TAM 4

int main(void) {
    int seqNum = 0;  // Inicia o número de sequência
    int inicioJanela = 0;  // Inicia a janela de início
    int fimJanela = JANELA_TAM - 1;  // Inicia a janela de fim
    bool ack[JANELA_TAM];  // Cria um array de reconhecimento
    for (int i = 0; i < JANELA_TAM; i++) {
        ack[i] = false;  // Inicializa todos os reconhecimentos como falsos
    }

    // Enquanto houver pacotes a serem enviados
    while (seqNum < 20) {
        // Envia pacotes na janela atual
        for (int i = inicioJanela; i <= fimJanela; i++) {
            // Simula o envio de pacotes
            printf("Pacote %d enviado\n", i);
        }

        // Recebe reconhecimentos dos pacotes enviados
        for (int i = inicioJanela; i <= fimJanela; i++) {
            int ackNum;
            // Simula o recebimento de reconhecimento
            printf("Entre com o número de reconhecimento para o pacote %d (0 para NACK, 1 para ACK): ", i);
            scanf("%d", &ackNum);
            if (ackNum == 0) {
                printf("Pacote %d foi NACK\n", i);
            } else {
                ack[i - inicioJanela] = true;
                printf("Pacote %d foi ACK\n", i);
            }
        }

        // Avança a janela até o primeiro NACK
        while (ack[0]) {
            inicioJanela++;
            fimJanela++;
            seqNum++;
            for (int i = 0; i < JANELA_TAM - 1; i++) {
                ack[i] = ack[i + 1];
            }
            ack[JANELA_TAM - 1] = false;
        }
    }

    return 0;
}

