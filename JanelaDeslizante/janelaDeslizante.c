#include <stdio.h>
#include <stdbool.h>

#define TAM_JANELA 5
#define MAX_PACOTE 10

bool janela[MAX_PACOTE];
int base = 0;
int prox_seq = 0;

void send_packet(int seq_num) {
    printf("Enviando pacote %d\n", seq_num);
    janela[seq_num % MAX_PACOTE] = true;
}

void receive_ack(int ack_num) {
    printf("Recebendo ACK %d\n", ack_num);
    int i;
    for (i = base; i < ack_num; i++) {
        janela[i % MAX_PACOTE] = false;
    }
    base = ack_num;
}

int main() {
    while (prox_seq < MAX_PACOTE) {
        if (prox_seq < base + TAM_JANELA) {
            send_packet(prox_seq);
            prox_seq++;
        }
        // Simulação da recepção de ACK
        int ack_num = base + TAM_JANELA / 2;
        receive_ack(ack_num);
    }
    return 0;
}
